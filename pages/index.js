import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import 'tailwindcss/tailwind.css'
import MainLayout from "@/components/layout/MainLayout";

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>Befriends-clinic</title>
      </Head>
      <MainLayout>

      </MainLayout>
    </>
  )
}
