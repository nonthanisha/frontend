import React from "react";
import Header from "../shared/Header";
import Footer from "../shared/Footer";
import Head from "next/head";

const MainLayout = ({ children }) => {
  return (
    <div className="flex flex-col justify-between h-screen">
      <Head>
        <title>Befriends-clinic</title>
      </Head>
      <Header />
      {children}
      <Footer />
    </div>
  );
}

export default MainLayout;
